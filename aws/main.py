import boto3

ec2_client = boto3.client('ec2', region_name="us-east-1")
ec2_resource = boto3.resource('ec2', region_name="us-east-1")

reservations = ec2_client.describe_instances()
for reservation in reservations['Reservations']:
    instances = reservation['Instances']
    for instance in instances:
        print(f"Status of instance {instance['InstanceId']} is: {instance['State']['Name']}")


statuces = ec2_client.describe_instance_status()
for status in statuces['InstanceStatuses']:
    ins_status = status['InstanceStatus']
    sys_status = status['SystemStatus']
    state = status['InstanceState']
    print(
        f"Instance {status['InstanceId']} is {state['Name']} with instance status is {ins_status['Status']} and system status is {sys_status['Status']}")
